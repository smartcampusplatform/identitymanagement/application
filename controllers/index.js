'use strict';

var IndexModel = require('../models/index')
const axios = require('axios')

module.exports = function (router) {

    var model = new IndexModel()

    router.get('/', function (req, res) {

        if(req.query.u) {
          var checktokenURI = 'http://178.128.104.74/identitymanagement/api/token/' + req.query.u
          // console.log(checktokenURI)
          axios.get(checktokenURI)
               .then(data => {
                 // console.log(data.data.messages)
                 if(data.data.data) {
                   res.render('index', model)
                 } else {
                   res.redirect('login')
                 }
               })
               .catch(err => {
                 res.status(500).send("Internal Server Error")
               })
        } else {
          res.redirect('login')
        }

    });

};
