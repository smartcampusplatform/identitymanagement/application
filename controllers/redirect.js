'use strict';


module.exports = function (router) {

    router.get('/', function (req, res) {

      var redirectVars
      if(req.query.url) {
        var urlTemp = req.query.url
        var url = decodeURIComponent(urlTemp)
        var firstSeparator = (url.indexOf('?')==-1 ? '?' : '&')

        redirectVars = {
          'u': req.query.user,
          'm': req.query.msg
        }
        var queryStringParts = new Array()
        for(var key in redirectVars) {
          if(redirectVars[key]) {
            queryStringParts.push(key + '=' + encodeURIComponent(redirectVars[key]))
          }
        }
        var queryString = queryStringParts.join('&')

        res.redirect(url + firstSeparator + queryString)
      } else {
        // res.send("400", "Bad request")
        res.status(400).send("Bad request");
      }

    });

};
