$(function(){

  $('#portfolio').mixitup({
    targetSelector: '.item',
    transitionSpeed: 450
  });

  $.urlParam = function(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
  }

  // console.log($.urlParam('u'));
  var getUserURI = 'http://178.128.104.74/identitymanagement/api/token/' + $.urlParam('u');
  // var getUserURI = 'http://localhost:8000/api/token/' + $.urlParam('u');
  // console.log(getUserURI);

  $.ajax({
      url: getUserURI,
      type: 'GET',
      encode: true
  })
  .done(function(res) {
    $("#userinfo").html('Welcome to Smartcampus Platform, ' + res.data.login_id);
    // console.log(res.data.login_id);
  });

  var formDataLogout = {
    'usertoken': $.urlParam('u')
  };
  $('#logout-btn').on('click', function() {
    $.confirm({
      title: 'Confirm!',
      boxWidth: '40%',
      useBootstrap: false,
      buttons: {
        confirm: {
          btnClass: 'btn-red',
          action: function() {
            $.ajax({
                url: 'http://178.128.104.74/identitymanagement/api/logout',
                // url: 'http://localhost:8000/api/logout',
                type: 'POST',
                data: formDataLogout,
                // dataType: 'application/json', // what type of data do we expect back from the server
                contentType: 'application/x-www-form-urlencoded',
                encode: true,
                success: function(res){
                  // console.log(res.messages);
                  if(res.messages == 'user logout successfully') {
                    // location.href = '/login';
                    localStorage.removeItem('tokenuser');
                    locationRedirect = '/login&msg=' + encodeURIComponent(res.messages);
                    location.href = '/redirect?url=' + locationRedirect;
                  } else {
                    locationRedirect = '/&msg=' + encodeURIComponent(res.messages);
                    location.href = '/redirect?url=' + locationRedirect;
                  }
                  // alert(JSON.stringify(res));
                },
                error: function(err){
                  // console.log('error: ', err);
                  // alert('error: ', err);
                  locationRedirect = '/&msg=' + encodeURIComponent('internal server error');
                  location.href = '/redirect?url=' + locationRedirect;
                }
            });
          }
        },
        cancel: function() {
          // $.alert('Canceled!');
        }
      }
    });
  });

  // console.log($( "li[id]" ).length);

  $('#newsmanagementsystemapp').on('click', function() {
    // var locationRedirect = encodeURIComponent('http://178.128.104.74:6150/?user=') + $.urlParam('u');
    location.href = 'http://178.128.104.74:6150/api/documentation?u=' + $.urlParam('u');
  });

  $('#smartclassroomapp').on('click', function() {
    // var locationRedirect = encodeURIComponent('http://178.128.104.74:6150/?user=') + $.urlParam('u');
    location.href = 'http://178.128.104.74/smartclassroomapp?u=' + $.urlParam('u');
  });

  /*
   *
  $.each($('li[id]'), function(index,value) {
    // console.log(index + ": " + value.id);
    $('#' + value.id).on('click', function() {
      var locationRedirect = encodeURIComponent('http://178.128.104.74/') + value.id + '&user=' + $.urlParam('u');
      location.href = '/redirect?url=' + locationRedirect;
    });
  });
   *
   */

});
