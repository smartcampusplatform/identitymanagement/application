(function ($) {

    "use strict";

    /*
     *
    if(localStorage.length > 0) {
      // console.log('ada local storage');
      if(localStorage.getItem('tokenuser')) {
        var tokenstored = localStorage.getItem('tokenuser');
        // console.log(tokenstored);
        var locationRedirect = '/&user=' + localStorage.getItem('tokenuser');
        // console.log(locationRedirect);
        location.href = '/redirect?url=' + locationRedirect;
      }
    }

    if(performance.navigation.type == 1) {
      // console.info("This page is reloaded");
      location.href = '/login';
    } else {
      // console.info("This page is not reloaded");
    }
     *
     */

    $.urlParam = function(name){
      var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
      if (results==null) {
         return null;
      }
      return decodeURI(results[1]) || 0;
    }

    if($.urlParam('m')) {
      if($.urlParam('m') != 'user logout successfully') {
        $("#loginmsg").html($.urlParam('m'));
      }
    }

    /*==================================================================
    [ Focus input ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })
    })

    $('#userpass').keypress(function (e) {
      var key = e.which;
      if(key == 13) {
        $('.validate-form').submit();
        return false;
      }
    });

    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(evt){
        evt.preventDefault();
        var check = true;
        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }
        if($('.alert-validate').length == 0) {
          var formDataLogin = {
            'userid': $('input[id=userid]').val(),
            'userpass': $('input[id=userpass]').val()
          };
          // data.append('userid', $('input[id=userid]').val());
          // data.append('userpass', $('input[id=userpass]').val());
          // console.log(formData);
          // alert(JSON.stringify(formData));
          $.ajax({
              // url: 'http://localhost:8000/api/login',
              url: 'http://178.128.104.74/identitymanagement/api/login',
              type: 'POST',
              data: formDataLogin,
              // dataType: 'application/json', // what type of data do we expect back from the server
              contentType: 'application/x-www-form-urlencoded',
              encode: true,
              success: function(res){
                // console.log(res);
                // alert(JSON.stringify(res));
                var locationRedirect;
                if(!res.messages.includes('login successfully')) {
                  locationRedirect = '/login&msg=' + encodeURIComponent(res.messages);
                  location.href = '/redirect?url=' + locationRedirect;
                } else {
                  // localStorage.setItem('tokenuser', res.data.login_token);
                  locationRedirect = '/&user=' + res.data.login_token;
                  location.href = '/redirect?url=' + locationRedirect;
                }
              },
              error: function(err){
                // console.log('error: ', err);
                // alert('error: ', err);
                locationRedirect = '/login&msg=' + encodeURIComponent('internal server error');
                location.href = '/redirect?url=' + locationRedirect;
              }
          });
        }
        return check;
    });

    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).removeClass('alert-validate');
    }

    /*==================================================================
    [ Show pass ]*/
    var showPass = 0;
    $('.btn-show-pass').on('click', function(){
        if(showPass == 0) {
            $(this).next('input').attr('type','text');
            $(this).addClass('active');
            showPass = 1;
        }
        else {
            $(this).next('input').attr('type','password');
            $(this).removeClass('active');
            showPass = 0;
        }
    });

})(jQuery);
